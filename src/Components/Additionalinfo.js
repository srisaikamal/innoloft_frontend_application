import React, { useState } from "react";
import { Form } from "antd";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import { makeStyles } from "@material-ui/core/styles";
import Container from "@material-ui/core/Container";
import Select from "@material-ui/core/Select";
import FormControl from "@material-ui/core/FormControl";
import { InputLabel, MenuItem } from "@material-ui/core";
import { Alert } from "antd";

const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(8),
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: "100%", // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
  formControl: {
    minWidth: 410,
  },
}));

const Additionalinfo = () => {
  const [form] = Form.useForm();
  const classes = useStyles();

  const [open, setOpen] = useState(false);

  const onFinish = (values) => {
    setOpen(true);
  };

  return (
    <>
      <Container component="main" maxWidth="xs">
        {open && (
          <Alert
            message="Success"
            description="Form submitted successfully"
            type="success"
            showIcon
          />
        )}

        <Form className={classes.form} onFinish={onFinish}>
          <Form.Item
            name="First Name"
            rules={[
              {
                required: true,
                message: "Please input your First Name",
              },
            ]}
            hasFeedback
          >
            <TextField margin="normal" fullWidth label="First Name" autoFocus />
          </Form.Item>

          <Form.Item
            name="Last Name"
            rules={[
              {
                required: true,
                message: "Please input your Last Name",
              },
            ]}
            hasFeedback
          >
            <TextField
              margin="normal"
              fullWidth
              label="Last Name"
              type="text"
            />
          </Form.Item>

          <Form.Item
            name="Address"
            rules={[
              {
                required: true,
                message: "Please input your Last Name",
              },
            ]}
            hasFeedback
          >
            <TextField margin="normal" fullWidth label="Address" type="text" />
          </Form.Item>

          <FormControl className={classes.formControl}>
            <InputLabel id="demo-simple-select-label">Age</InputLabel>
            <Select labelId="demo-simple-select-label" id="demo-simple-select">
              <MenuItem value={10}>Germany</MenuItem>
              <MenuItem value={20}>Austria</MenuItem>
              <MenuItem value={30}>Switzerland </MenuItem>
            </Select>
          </FormControl>

          <Button
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            style={{ marginTop: "20px" }}
          >
            Update
          </Button>
        </Form>
      </Container>
    </>
  );
};

export default Additionalinfo;
